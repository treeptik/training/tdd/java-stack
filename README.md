# TDD Java Stack

[![(c) Treeptik, Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

Demonstration of Test Driven Development of a stack structure in Java.

## License

See [License](LICENSE.md).

