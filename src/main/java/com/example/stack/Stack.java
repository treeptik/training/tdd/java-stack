package com.example.stack;

import java.util.ArrayList;
import java.util.List;

public class Stack<T> {
	private List<T> contents = new ArrayList<>();

	public boolean isEmpty()
	{
		return contents.isEmpty();
	}

	public void push(T x)
	{
		contents.add(x);
	}

	public T pop()
	{
		if (isEmpty()) {
			throw new IllegalStateException("pop: empty stack");
		}
		
		return contents.remove(contents.size() - 1);
	}

}