package com.example.stack;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Stack")
public class StackTest {
	private Stack<Integer> stack;
	
	@DisplayName("Given an empty stack")
	@Nested
	public class GivenEmpty {
		@BeforeEach
		public void given() {
			stack = new Stack<>();
		}
		
		@DisplayName("then the stack is empty")
		@Test
		public void thenIsEmpty() {
			assertTrue(stack.isEmpty());
		}

		@DisplayName("then pop throws an exception")
		@Test
		public void whenPop_thenException() {
			Assertions.assertThrows(IllegalStateException.class, () -> {
				stack.pop();
			});
		}
		
		@DisplayName("when pushing an element")
		@Nested
		public class WhenPush {
			@BeforeEach
			public void when() {
				stack.push(3);
			}
			
			@DisplayName("then stack is not empty")
			@Test
			public void thenNotEmpty() {
				assertFalse(stack.isEmpty());
			}
			
			@DisplayName("then pop returns the element")
			@Test
			public void thenPopReturnsElement() {
				assertEquals(3, stack.pop());
			}
		}
	}
	
	@DisplayName("Given a singleton stack")
	@Nested
	public class GivenSingleton {
		@BeforeEach
		public void given() {
			stack = new Stack<>();
			stack.push(2);
		}
		
		@DisplayName("when popping")
		@Nested
		public class WhenPop {
			int poppedElement;
			
			@BeforeEach
			public void when() {
				poppedElement = stack.pop();
			}
			
			@DisplayName("then stack is empty")
			@Test
			public void thenIsEmpty() {
				assertTrue(stack.isEmpty());
			}
			
			@DisplayName("then popped element is same")
			public void thenPoppedElementSame() {
				assertEquals(2, poppedElement);
			}
		}
	}

	@DisplayName("Given a non empty stack")
	@Nested
	public class GivenNonEmpty {
		@BeforeEach
		public void given() {
			stack = new Stack<>();
			stack.push(1);
			stack.push(2);
			stack.push(3);
		}
		
		@DisplayName("When pushing an element")
		@Nested
		public class WhenPush {
			@BeforeEach
			public void when() {
				stack.push(4);
			}
			
			@Test
			public void thenPoppedElementSame() {
				assertEquals(4, stack.pop());
			}
			
			@Test
			public void thenPreviousElementsRemain() {
				stack.pop();
				assertEquals(3, stack.pop().intValue());
				assertEquals(2, stack.pop().intValue());
				assertEquals(1, stack.pop().intValue());
			}
			
			@Test
			public void thenIsNotEmpty() {
				assertFalse(stack.isEmpty());
			}
		}
		
	}
	
}